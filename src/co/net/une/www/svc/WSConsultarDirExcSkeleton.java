
/**
 * WSConsultarDirExcSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WSConsultarDirExcSkeleton java skeleton for the axisService
     */
    public class WSConsultarDirExcSkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WSConsultarDirExcLocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param codigoSolicitud
                                     * @param codigoSistema
                                     * @param codigoDireccion
         */
        

                 public co.net.une.www.gis.WSConsultarDirExcRSType consultarDirExc
                  (
                  co.net.une.www.gis.BoundedString15 codigoSolicitud,co.net.une.www.gis.BoundedString5 codigoSistema,co.net.une.www.gis.BoundedString100 codigoDireccion
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("codigoSolicitud",codigoSolicitud);params.put("codigoSistema",codigoSistema);params.put("codigoDireccion",codigoDireccion);
		try{
		
			return (co.net.une.www.gis.WSConsultarDirExcRSType)
			this.makeStructuredRequest(serviceName, "consultarDirExc", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    